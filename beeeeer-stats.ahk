﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

IniRead, inputAddress, stats.ini, stats, address, %A_space%

Gui, font, Arial s11
Gui +AlwaysOnTop
Gui, Add, Text, w450 x+20 Center, b(e^5)r stats
Gui, font, Arial s9
Gui, Add, Text, w120 Section Right, Address:
Gui, Add, Edit, w250 vAddress ys R1 -wrap, %inputAddress%
Gui, Add, Text, w100 xs y+20 Center Section , total reward:
Gui, Add, Edit, w120 Readonly ys-2 Center vtotalReward,
Gui, Add, Text, w100 ys Center, total pay:
Gui, Add, Edit, w120 Readonly ys-2 Center vtotalPayout,
Gui, Add, Text, w100 Center Section xs, total fee:
Gui, Add, Edit, w120 ReadOnly ys-2 Center vtotalFee,
Gui, Add, Text, w100 Center ys, fee paid:
Gui, Add, Edit, w120 ReadOnly ys-2 Center vtotalFeeOut,
Gui, Add, Text, w100 Center Section xs y+20, net reward:
Gui, Add, Edit, w120 ReadOnly ys-2 center vnetReward
Gui, Add, Text, w100 Center ys, net pay:
Gui, Add, Edit, w120 ReadOnly ys-2 center vnetPay
Gui, Add, Text, w100 Center Section xs+100 y+20, remaining balance:
Gui, Add, Edit, w120 ReadOnly Center ys-2 vnetBalance 
GoSub, getStats 
Gui, show, Autosize, b(e^5)r stats
SetTimer, getStats, 60000
Return

Guiclose:
Gui, Submit
IniWrite, %Address%, stats.ini, stats, address
ExitApp

getStats:
Gui, Submit, NoHide
If !Address
{
	Msgbox, 0, b(e^5)r stats, Enter your payment address then restart
	Return
}
URLDownloadToFile, http://beeeeer.org/user/%Address%, xpm-stats
If ErrorLevel
{
	Msgbox, 0, b(e^5)r stats, something went wrong contacting b(e^5)r.`nCheck your address is correct and restart
	Return
}
totalReward := 0
totalFee := 0
totalPayout := 0
totalFeeOut := 
Loop, Read, xpm-stats
{
	if InStr(A_LoopReadLine, "reward:")
	{
		stats := StrSplit(A_LoopReadLine, "reward: ")
		Loop, % stats.MaxIndex()
		{
			If InStr(stats[A_Index], "fee:")
			{
				If InStr(stats[A_Index], "@")
				{
					pay := SubStr(stats[A_Index], 1, 7)
					pay = %pay%
					feeOutPos := InStr(stats[A_Index], "fee: ")
					feeOut := SubStr(stats[A_Index], (feePos+4), 9)
					feeOut = %fee%
					
					totalPayout += pay
					totalFeeOut += feeOut
					Continue
				}
				reward := SubStr(stats[A_Index], 1, 7)
				reward = %reward%
				feePos := InStr(stats[A_Index], "fee: ")
				fee := SubStr(stats[A_Index], (feePos+4), 9)
				fee = %fee%
				
				totalReward += reward
				totalFee += fee
			}
		}
	}
}
GuiControl, , totalReward, % totalReward
GuiControl, , totalFee, % totalFee
GuiControl, , netReward, % (totalReward-totalFee)
GuiControl, , totalPayout, % totalPayout
Guicontrol, , totalFeeOut, % totalFeeOut
GuiControl, , netPay, % (totalPayout-totalFeeOut)
GuiControl, , netBalance, % (totalReward-totalFee)-(totalPayout-totalFeeOut)
Return
